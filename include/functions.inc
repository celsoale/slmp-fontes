<?php
require_once("config.inc");
require_once("class.db.inc");

function printStatus($status, $msg){
	if($status == "error"){
		print "<font color='red'>";
	}elseif($status == "ok"){
		print "<font color='green'>";
	}elseif($status == "info"){
		print "<font color='gray'>";
	}
	print "<b>".$msg."</b></font><br>\n";
}
?>
