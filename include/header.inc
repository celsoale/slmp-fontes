<html>
	<head>
		<title>Fontes</title>
		<script language="javascript">
			var last_element = null;
			function new_theme(){
				window.open("themes.php","Temas", "height=600, width=500, location=no, menubar=no, resizeable=yes, toolbar=no");
				return false;
			}
			function new_search(texto){
				console.log("nova_procura: "+texto);
				window.open("search.php?search="+texto,"Procura", "height=600, width=500, location=no, menubar=no, resizeable=yes, toolbar=no");
				return false;
			}
			function confirm_exclusion(element){
				var retCode = false;
				if(element.value == "EXCLUIR"){
					retCode = confirm("Tem certeza que deseja excluir?");
				}
				if(element.value == "DESCARTAR"){
					retCode = confirm("Tem certeza que deseja descartar modificacoes?");
				}
				return retCode;
			}
			function setSelectedResult(element){
				if(last_element != null){
					last_element.style.backgroundColor='white';
				}
				element.style.backgroundColor='grey';
				last_element = element;
				setParentId(element.id);
			}
				
			function setParentId(id){
				window.opener.location = "index.php?refresh_with_id="+id;
			}
		
			function parentDoExclusion(){
				if (last_element == null){
					alert("Selecione um elemento!");
					return;
				}

				if(confirm_exclusion() == true){
					window.opener.location = "index.php?do_exclusion="+last_element.id;
					//window.location.reload(true);
					last_element.parentNode.removeChild(last_element);
					last_element= null;
				}
			}
			function setSelectedTheme(element){
				if(last_element != null){
					last_element.style.backgroundColor='white';
				}
				element.style.backgroundColor='grey';
				last_element = element;
			}
				
			function parentAddTheme(){
				if(last_element == null){
					alert("Selecione um elemento!");
					return;
				}
				var last_action = opener.document.getElementById('last_action');
				if(last_action.value != 'NOVO' && last_action.value != 'EDITAR'){
					alert("Edicao desabilitada na pagina principal.");
					return;
				}
				var temas = opener.document.getElementById('temas');
				temas.value+= last_element.id+"\r\n";
				//temas.disabled = false;

				//var temas_mod = opener.document.getElementById('temas_mod');
				//temas_mod.innerHTML = "<font color='red'><b>(modificado mas n&atilde;o salvo!)</b></font>";

			}
		</script>
	</head>
<body>
